from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveAPIView

from blog.models import Blog, Comment
from blog.serializers import BlogSerializer, CommentSerializer


class BlogCreateListAPIView(ListAPIView, RetrieveAPIView, CreateAPIView):
    """
    Create/List blog api_view
    """
    serializer_class = BlogSerializer
    queryset = Blog.objects.all()


class BlogRetrieveAPIView(RetrieveAPIView):
    """
    Retrieve blog api_view
    """
    serializer_class = BlogSerializer
    queryset = Blog.objects.all()


class CommentAPIView(CreateAPIView):
    """
    Create comment api_view
    """
    serializer_class = CommentSerializer
    queryset = Comment.objects.all()

    def create(self, request, *args, **kwargs):
        request.data._mutable = True
        request.data.update(kwargs)
        request.data._mutable = False
        return super().create(request, *args, **kwargs)



