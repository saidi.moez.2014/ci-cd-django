from django.urls import reverse
from model_bakery import baker
from rest_framework.test import APITestCase


class TestBlog(APITestCase):
    BLOG_COUNT = 10

    def setUp(self):
        blogs = baker.make("blog.blog", _quantity=self.BLOG_COUNT)
        baker.make("blog.comment", _quantity=10, blog=blogs[0])

    def test_list_blog(self):
        """
        Add test for blog
        """
        resp = self.client.get(reverse('blog:list_create_blog'))
        self.assertEqual(len(resp.data), self.BLOG_COUNT)

    def test_create_blog(self):
        """
        Add test for blog
        """
        data = {"title": "Hi",
                "description": "CC"}
        resp = self.client.post(reverse('blog:list_create_blog'), data=data)
        self.assertDictContainsSubset(data, resp.data)

