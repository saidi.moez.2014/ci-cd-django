from django.db import models


class Blog(models.Model):
    """
    Create blog model with corresponding requirements
    """
    title = models.CharField(max_length=50)
    description = models.TextField()
    image = models.ImageField(blank=True, null=True)

    def __str__(self):
        return f"{self.id} : {self.title}"


class Comment(models.Model):
    """
    Comment blog model with corresponding requirements
    """
    blog = models.ForeignKey(Blog, on_delete=models.CASCADE, related_name='comments')
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.id} : {self.blog.title} : {self.text}"
