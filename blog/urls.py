from django.urls import path

from blog import views

app_name = "blog"
urlpatterns = [
    path('blogs/', views.BlogCreateListAPIView.as_view(), name='list_create_blog'),
    path('blogs/<int:pk>', views.BlogRetrieveAPIView.as_view(), name='retrieve_blog'),
    path('blogs/<int:blog>/comment', views.CommentAPIView.as_view(), name='comment')
]
